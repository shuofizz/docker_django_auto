import os

from django.contrib import admin
from django.contrib.admin import helpers
from django.http import FileResponse
from django.shortcuts import HttpResponseRedirect, HttpResponse
from django.urls import path
from django.utils.html import format_html

from DBModel.models import PlatformsData, PlatformsData_Vi, PlatformsData_Sp, PlatformsVnDeploy, PlatformsData_Ph, PlatformsData_Zh
from DBModel import views
from django.forms import Textarea, TextInput
from UploadImg.forms import *
from UploadImg.views import upload
import requests
import zipfile
from django.views.decorators.csrf import csrf_exempt
import time
from django.core import serializers

# Register your models here.
@admin.register(PlatformsData)
class AppData(admin.ModelAdmin):
    change_list_template = 'DBModel/changelist.html'
    delete_confirmation_template = 'DBModel/delete_confirmation.html'
    add_form_template = 'DBModel/add_form.html'
    change_form_template = 'DBModel/change_form.html'


    list_display = ['code', 'ios_appname', 'android_appname', 'icon_upload', 'regular_customer', 'hyper_link', 'remark', 'remark2', 'android_version', 'ios_version', 'android_app_id', 'ios_app_id',
                    'ios_master_secret', 'ios_app_key', 'android_master_secret', 'android_app_key', 'open_install_key', 'save']

    # readonly_fields = ['code']
    ordering = ('code',)
    formfield_overrides = {
        models.CharField : {'widget': TextInput(attrs={'size': 20})},
        models.TextField : {'widget': Textarea(attrs={'rows':2, 'cols':40})},
    }
    # # Action选项都是在页面上方显示
    actions_on_top = False
    # Action选项都是在页面下方显示
    actions_on_bottom = False
    # # 是否显示选择个数
    # actions_selection_counter = True
    clean_cache_code = ""
    def save(self, obj):
        return format_html(
            "<input type='submit' name='do_save_{}' value='儲存'>",
            obj.id,
        )

    def icon_upload(self, obj):
        if self.clean_cache_code == obj.code:
            tstr = time.time()
            IconSrc = f"/media/images/{obj.code}/icon.png?{tstr}"
            LaunchSrc = f"/media/images/{obj.code}/launchscreen.png?{tstr}"
            CircleSrc = f"/media/images/{obj.code}/circle_icon.png?{tstr}"
            PreSrc = f"/media/images/{obj.code}/launchscreen_resize.png?{tstr}"
            PreIconSrc = f"/media/images/{obj.code}/icon_resize.png?{tstr}"
            PreCircleSrc = f"/media/images/{obj.code}/circle_icon_resize.png?{tstr}"
            self.clean_cache_code = ""
        else:
            IconSrc = f"/media/images/{obj.code}/icon.png"
            LaunchSrc = f"/media/images/{obj.code}/launchscreen.png"
            CircleSrc = f"/media/images/{obj.code}/circle_icon.png"
            PreSrc = f"/media/images/{obj.code}/launchscreen_resize.png"
            PreIconSrc = f"/media/images/{obj.code}/icon_resize.png"
            PreCircleSrc = f"/media/images/{obj.code}/circle_icon_resize.png"

        spaces = '&nbsp;' * 40

        return  format_html(
            '''<a href="{}" target="_blank"><img src="{}" width='30' height='30'></a>
            <a href="{}" target="_blank"><img src="{}" width='30' height='51'></a>
            <a href="{}" target="_blank"><img src="{}" width='30' height='30'></a>
            <button class='showUploadDialog' type='button' name='upload' value='{}'>upload</button>''' + spaces,
            IconSrc, PreIconSrc, LaunchSrc, PreSrc, CircleSrc, PreCircleSrc, obj.code
        )
    icon_upload.short_description = 'Icon/背景圖'

    def hyper_link(self, obj):
        link = obj.download_url.split('://')
        links = [s.strip() for s in link]

        if 'http' not in links and 'https' not in links:
            url = 'http://' + link[-1].replace(" ", "")
        else:
            url = obj.download_url

        return format_html(
            '''<a href="{}" target="_blank">{}</a>''',
            url, url
        )
    hyper_link.short_description = '域名'

    #依照權限顯示list, enable edit list
    def changelist_view(self, request, extra_context=None):
        ##GitLab API獲取版號
        ios_opts = []
        android_opts = []
        platform = self.get_platform()
        if self.model == PlatformsData or self.model == PlatformsData_Vi or self.model == PlatformsData_Ph or self.model == PlatformsData_Zh:
            android_versions = views.get_platform_version(self.model, 1)
            ios_versions = views.get_platform_version(self.model, 0)

            ios_selector = self.model._meta.get_field('ios_version')
            android_selector = self.model._meta.get_field('android_version')
            ios_selector.choices = ios_versions
            ios_selector.default = ios_versions[0]
            android_selector.choices = android_versions
            android_selector.default = android_versions[0]

            ios_opts = list(f[0] for f in ios_versions)
            android_opts = list(f[0] for f in android_versions)

        user = request.user
        if user.is_superuser:
            self.list_editable = ['remark','android_version','ios_version']
        else:
            self.list_editable = ['download_url','remark']
            can_not_editable = [f for f in self.list_display if not f in ['download_url', 'remark']]
            self.readonly_fields = can_not_editable

        objects = self.model.objects.all()
        json_serializer = serializers.get_serializer("json")()
        companies = json_serializer.serialize(self.model.objects.all())

        form = IconForm()
        context = {'form':form, 'ios_opts': ios_opts, 'android_opts': android_opts, 'platform': platform, 'data_list': objects, 'data_parse': companies }
        return super(AppData, self).changelist_view(request, extra_context=context)

    #Custom Add_form
    def add_view(self, request, form_url='', extra_context=None):
        objects = self.model.objects.all()
        json_serializer = serializers.get_serializer("json")()
        companies = json_serializer.serialize(self.model.objects.all())
        context = {'data_list': objects, 'data_parse': companies }
        return super(AppData, self).add_view(request, extra_context=context)

    # 批量更新
    def get_urls(self):
        urls = super().get_urls()
        my_urls = [
            path('post_tools/', self.post_tools),
            path('upload_img/', self.upload_img),
            # path('my-ajax-test/', self.set_ajax),
            path('my-ajax-test/', self.set_ajax, name='ajax-test-view'),
            path('downloadfile/', self.download_file, name='downloadfile'),
            path('tt/', self.tt)
        ]
        return my_urls + urls

    def get_platform(self):
        if self.model == PlatformsData:
            return  "cn"
        elif self.model == PlatformsData_Vi:
            return  "vn"
        elif self.model == PlatformsVnDeploy:
            return  "vn_deploy"
        elif self.model == PlatformsData_Ph:
            return "ph"
        elif self.model == PlatformsData_Zh:
            return "zh"
        else:
            return "sp"

    def post_tools(self, request):
        selected = request.POST.getlist(helpers.ACTION_CHECKBOX_NAME)
        objects = self.model.objects.filter(id__in=selected)

        print('post_tools',request.POST)
        querys = request.POST.lists()
        # print(querys)
        print(selected)

        if 'buttonAction' in request.POST:
            button_state = request.POST['buttonAction']
            if button_state == 'archive':
                views.packer(self, request, objects, self.model)
                print('archiving')

            elif button_state == 'saved':
                ios_update_version = request.POST['ios_version']
                ios_check = self.check_input(ios_update_version, selected)
                android_update_version = request.POST['android_version']
                android_check = self.check_input(android_update_version, selected)

                if ios_check[0] == True:
                    objects.update(ios_version=ios_update_version)
                if android_check[0] == True:
                    objects.update(android_version=android_update_version)

            elif button_state == 'cancel':
                return HttpResponseRedirect("../")

            return HttpResponseRedirect("../")

        elif any("do_save" in s for s in request.POST):
            ss = filter(lambda x: 'do_save' in x, request.POST)
            id = str(list(ss)[0]).split('_')[-1]

            querys = request.POST
            key = next((k for k, v in querys.items() if v == id), None)
            key_id = key.split('-')[-2]

            android_version = request.POST[f'form-{key_id}-android_version']
            ios_version = request.POST[f'form-{key_id}-ios_version']
            remark = request.POST[f'form-{key_id}-remark']

            object = self.model.objects.filter(id=id)
            object.update(remark=remark, android_version=android_version, ios_version=ios_version)
            print('do save now. will sync jenkins!')
            views.sync_jenkins(self.model, ios_version, android_version, object[0].code)

        self.message_user(request, "Selects has been update")
        return HttpResponseRedirect("../")

    # 上傳圖片
    def upload_img(self, request):
        form = IconForm(request.POST, request.FILES)
        if form.is_valid():
            self.clean_cache_code = upload(self, request)

        return HttpResponseRedirect("../")

    @csrf_exempt
    def set_ajax(self, request):
        print('ajax request')
        print(request.POST.getlist('check'))
        selected = request.POST.getlist(helpers.ACTION_CHECKBOX_NAME)
        print(request.POST)
        # self.message_user(request, "All heroes are now mortal")
        return HttpResponse('ajax response')

    @csrf_exempt
    def tt(self, request):
        dict = {"name":"yyy"}
        context = dict
        print(context)

        return HttpResponseRedirect("../", context)

    # 檢查Input
    def check_input(self, update_version, selected):
        if len(selected) == 0:
            return False, 'Should select any item'
        if len(update_version) < 6:
            return False, 'version error'
        return True, ''

    def download_file(self, request):
        print('download', request)
        file_path = requests.get('http://172.30.12.247:8080/AppConfigs/android-new-native/config.json')
        response = FileResponse(file_path, as_attachment=True, filename='file_name.json')
        response['Content-Disposition'] = 'attachment; filename=config.json'


        zip_file = zipfile.ZipFile("response.zip", 'w')
        if os.path.isfile('http://172.30.12.247:8080/AppConfigs/android-new-native/config.json'):
            print('ya')
        else:
            print('not file')

        with requests.get('http://172.30.12.247:8080/AppConfigs/android-new-native/config.json', stream=True) as r:
            r.raise_for_status()
            print(r)
            with open('config.json', 'rb') as f:
                print(f)

        # response = HttpResponse(content_type='application/zip')
        # response['Content-Disposition'] = 'attachment; filename={}'.format("response.zip")
        return response

@admin.register(PlatformsData_Vi)
class VnData(AppData):
    list_display = ['code', 'ios_appname', 'android_appname', 'icon_upload', 'regular_customer', 'hyper_link', 'hyper_link2', 'remark', 'remark2', 'appearance_style', 'android_version', 'ios_version', 'android_app_id', 'ios_app_id',
                    'ios_master_secret', 'ios_app_key', 'android_master_secret', 'android_app_key', 'open_install_key', 'save']
    # readonly_fields = ['code']

    def hyper_link2(self, obj):
        try:
            link = obj.download_url2.split('://')
            links = [s.strip() for s in link]
            print(links)
            if 'http' not in links and 'https' not in links:
                url = 'http://' + link[-1].replace(" ", "")
            else:
                url = obj.download_url2
        except:
            url = ""

        return format_html(
            '''<a href="{}" target="_blank">{}</a>''',
            url, url
        )
    hyper_link2.short_description = '域名2'

@admin.register(PlatformsVnDeploy)
class VnData_deploy(AppData):
    list_display = ['code', 'ios_appname', 'android_appname', 'icon_upload', 'regular_customer', 'hyper_link', 'hyper_link2', 'remark', 'remark2', 'android_version', 'ios_version', 'android_app_id', 'ios_app_id',
                    'ios_master_secret', 'ios_app_key', 'android_master_secret', 'android_app_key', 'open_install_key', 'save']
    # readonly_fields = ['code']

    def hyper_link2(self, obj):
        try:
            link = obj.download_url2.split('://')
            links = [s.strip() for s in link]
            if 'http' not in links and 'https' not in links:
                url = 'http://' + link[-1].replace(" ", "")
            else:
                url = obj.download_url2
        except:
            url = ""

        return format_html(
            '''<a href="{}" target="_blank">{}</a>''',
            url, url
        )
    hyper_link2.short_description = '域名2'

@admin.register(PlatformsData_Ph)
class PhData(AppData):
    list_display = ['code', 'ios_appname', 'android_appname', 'icon_upload', 'regular_customer', 'hyper_link', 'hyper_link2', 'remark', 'remark2', 'appearance_style', 'android_version', 'ios_version', 'android_app_id', 'ios_app_id',
                    'ios_master_secret', 'ios_app_key', 'android_master_secret', 'android_app_key', 'open_install_key', 'save']
    # readonly_fields = ['code']

    def hyper_link2(self, obj):
        try:
            link = obj.download_url2.split('://')
            links = [s.strip() for s in link]
            print(links)
            if 'http' not in links and 'https' not in links:
                url = 'http://' + link[-1].replace(" ", "")
            else:
                url = obj.download_url2
        except:
            url = ""

        return format_html(
            '''<a href="{}" target="_blank">{}</a>''',
            url, url
        )
    hyper_link2.short_description = '域名2'

@admin.register(PlatformsData_Zh)
class ZhData(AppData):
    list_display = ['code', 'ios_appname', 'android_appname', 'icon_upload', 'regular_customer', 'hyper_link', 'hyper_link2', 'remark', 'remark2', 'appearance_style', 'android_version', 'ios_version', 'android_app_id', 'ios_app_id',
                    'ios_master_secret', 'ios_app_key', 'android_master_secret', 'android_app_key', 'open_install_key', 'save']
    # readonly_fields = ['code']

    def hyper_link2(self, obj):
        try:
            link = obj.download_url2.split('://')
            links = [s.strip() for s in link]
            print(links)
            if 'http' not in links and 'https' not in links:
                url = 'http://' + link[-1].replace(" ", "")
            else:
                url = obj.download_url2
        except:
            url = ""

        return format_html(
            '''<a href="{}" target="_blank">{}</a>''',
            url, url
        )
    hyper_link2.short_description = '域名2'

@admin.register(PlatformsData_Sp)
class SpData(AppData):
    list_display = ['code', 'ios_appname', 'android_appname', 'hyper_link', 'hyper_link2' , 'remark', 'android_version', 'ios_version', 'android_app_id',
                    'ios_app_id', 'ios_master_secret', 'ios_app_key', 'android_master_secret', 'android_app_key', 'open_install_key','save']
    readonly_fields = ['code']

    def hyper_link2(self, obj):
        try:
            link = obj.download_url2.split('://')
            links = [s.strip() for s in link]
            print(links)
            if 'http' not in links and 'https' not in links:
                url = 'http://' + link[-1].replace(" ", "")
            else:
                url = obj.download_url2
        except:
            url = ""

        return format_html(
            '''<a href="{}" target="_blank">{}</a>''',
            url, url
        )
    hyper_link2.short_description = '域名2'