from django.db import models

signs = (
    ('', '-'),
    ('企業簽', '企業簽'),
    ('代簽', '代簽'),
    ('超級簽', '超級簽'),
    ('代簽+超級簽', '代簽+超級簽'),
)

appearances = (
    ('day', '日間'),
    ('night', '夜間')
)

themes_main = (
    ('vn_origin', 'vn_origin'),
    ('vn_custom', 'vn_custom'),
    ('vn_custom_c', 'vn_custom_c'),
    ('vn_origin_b', 'vn_origin_b'),
    ('vn_origin_c', 'vn_origin_c'),
    ('vn_origin_e', 'vn_origin_e'),
    ('vn_origin_g', 'vn_origin_g'),
    ('vndf2', 'vndf2'),
    ('ph_777', 'ph_777'),
    ('ph_origin_d', 'ph_origin_d'),
    ('ph_lucky', 'ph_lucky'),
    ('ph_number', 'ph_number'),
    ('ph_origin_f', 'ph_origin_f')
)

xcode_target = (
    ('DSCaipiao', 'DSCaipiao'),
	('PHClassic', 'PHClassic'),
	('CNClassic', 'CNClassic')
)

# Create your models here.
class PlatformsData(models.Model):
    code = models.CharField(max_length= 100, verbose_name="平台名稱(Code)")
    platform_code = models.CharField(max_length= 100, verbose_name="平台代碼(Platform code)")
    ios_appname = models.CharField(max_length= 100)
    android_appname = models.CharField(max_length= 100)
    download_url = models.TextField(verbose_name="域名")
    remark = models.CharField(choices=signs, default=signs[0], verbose_name="簽名方式", null=True, blank=True, max_length= 100)
    remark2 = models.TextField(verbose_name="----註解----",null=True, blank=True)
    super_user_id = models.CharField(max_length= 100, null=True, blank=True)
    regular_customer = models.BooleanField(verbose_name="老客戶")
    android_version = models.CharField(max_length=100)
    ios_version = models.CharField(max_length=100)
    android_app_id = models.TextField()
    ios_app_id = models.TextField()
    ios_master_secret = models.TextField()
    ios_app_key = models.TextField()
    android_master_secret = models.TextField()
    android_app_key = models.TextField()
    open_install_key = models.TextField(null=True, blank=True)
    android_mini_version = models.TextField(null=True, blank=True)
    ios_mini_version = models.TextField(null=True, blank=True)
    android_short_version = models.TextField(null=True, blank=True)
    ios_short_version = models.TextField(null=True, blank=True)

    class Meta:
        # managed = False
        verbose_name = '平台'
        verbose_name_plural = '中文版App'
        db_table = 'platforms_cn'
        permissions = (('can_edit_field', 'Can_edit_field'),)


class PlatformsData_Vi(models.Model):
    code = models.CharField(max_length= 100, verbose_name="平台名稱(Code)")
    platform_code = models.CharField(max_length= 100, verbose_name="平台代碼(Platform code)")
    ios_appname = models.CharField(max_length=100)
    android_appname = models.CharField(max_length=100)
    download_url = models.TextField(verbose_name="域名")
    download_url2 = models.TextField(verbose_name="域名2", null=True, blank=True)
    remark = models.CharField(choices=signs, default=signs[0], verbose_name="簽名方式", null=True, blank=True, max_length= 100)
    remark2 = models.TextField(verbose_name="----註解----",null=True, blank=True)
    super_user_id = models.CharField(max_length= 100, null=True, blank=True)
    invite_code = models.CharField(max_length= 100, null=True, blank=True)
    regular_customer = models.BooleanField(verbose_name="老客戶") 
    is_test_domain = models.BooleanField(verbose_name="測試用平台")
    country_theme = models.CharField(max_length=100)
    plat_default_lang = models.CharField(max_length=100)
    appearance_style = models.CharField(choices=appearances, default=appearances[0], verbose_name="外觀樣式", max_length= 100)
    app_theme = models.CharField(choices=themes_main, default=themes_main[0], verbose_name="首頁模版", max_length=100)
    android_version = models.CharField(max_length=100)
    ios_version = models.CharField(max_length=100)
    android_app_id = models.TextField()
    ios_app_id = models.TextField()
    ios_master_secret = models.TextField()
    ios_app_key = models.TextField()
    android_master_secret = models.TextField()
    android_app_key = models.TextField()
    open_install_key = models.TextField(null=True, blank=True)
    android_mini_version = models.TextField(null=True, blank=True)
    ios_mini_version = models.TextField(null=True, blank=True)
    android_short_version = models.TextField(null=True, blank=True)
    ios_short_version = models.TextField(null=True, blank=True)
    ios_xcode_target = models.TextField(choices=xcode_target, default=xcode_target[0], verbose_name="XCode Target", max_length=100)
    android_gradle_module = models.TextField()
    android_appsflyer_dev_code = models.TextField(null=True, blank=True)    
    android_market_code = models.TextField(null=True, blank=True)   

    class Meta:
        verbose_name = '平台'
        verbose_name_plural = '越南版App'
        db_table = 'platforms_vn'


class PlatformsVnDeploy(models.Model):
    code = models.CharField(max_length= 100, verbose_name="平台名稱(Code)")
    platform_code = models.CharField(max_length= 100, verbose_name="平台代碼(Platform code)")
    ios_appname = models.CharField(max_length=100)
    android_appname = models.CharField(max_length=100)
    download_url = models.TextField(verbose_name="域名")
    download_url2 = models.TextField(verbose_name="域名2", null=True, blank=True)
    remark = models.CharField(choices=signs, default=signs[0], verbose_name="簽名方式", null=True, blank=True, max_length= 100)
    remark2 = models.TextField(verbose_name="----註解----",null=True, blank=True)
    super_user_id = models.CharField(max_length= 100, null=True, blank=True)
    regular_customer = models.BooleanField(verbose_name="老客戶")
    is_test_domain = models.BooleanField(verbose_name="測試用平台")
    country_theme = models.CharField(max_length=100)
    appearance_style = models.CharField(choices=appearances, default=appearances[0], verbose_name="外觀樣式", max_length= 100)
    app_theme = models.CharField(choices=themes_main, default=themes_main[0], verbose_name="首頁模版", max_length=100)
    android_version = models.CharField(max_length=100)
    ios_version = models.CharField(max_length=100)
    android_app_id = models.TextField()
    ios_app_id = models.TextField()
    ios_master_secret = models.TextField()
    ios_app_key = models.TextField()
    android_master_secret = models.TextField()
    android_app_key = models.TextField()
    open_install_key = models.TextField(null=True, blank=True)
    android_mini_version = models.TextField(null=True, blank=True)
    ios_mini_version = models.TextField(null=True, blank=True)
    android_short_version = models.TextField(null=True, blank=True)
    ios_short_version = models.TextField(null=True, blank=True)
    android_appsflyer_dev_code = models.TextField(null=True, blank=True)
    android_market_code = models.TextField(null=True, blank=True)
    android_market_mode = models.BooleanField(verbose_name="功能隱藏模式")

    class Meta:
        verbose_name = '平台'
        verbose_name_plural = '越南版安卓上架'
        db_table = 'platforms_vn_deploy'


class PlatformsData_Ph(models.Model):
    code = models.CharField(max_length= 100, verbose_name="平台名稱(Code)")
    platform_code = models.CharField(max_length= 100, verbose_name="平台代碼(Platform code)")
    ios_appname = models.CharField(max_length=100)
    android_appname = models.CharField(max_length=100)
    download_url = models.TextField(verbose_name="域名")
    download_url2 = models.TextField(verbose_name="域名2", null=True, blank=True)
    remark = models.CharField(choices=signs, default=signs[0], verbose_name="簽名方式", null=True, blank=True, max_length= 100)
    remark2 = models.TextField(verbose_name="----註解----",null=True, blank=True)
    super_user_id = models.CharField(max_length= 100, null=True, blank=True)
    invite_code = models.CharField(max_length= 100, null=True, blank=True)
    regular_customer = models.BooleanField(verbose_name="老客戶") 
    is_test_domain = models.BooleanField(verbose_name="測試用平台")
    country_theme = models.CharField(max_length=100)
    plat_default_lang = models.CharField(max_length=100)
    appearance_style = models.CharField(choices=appearances, default=appearances[0], verbose_name="外觀樣式", max_length= 100)
    app_theme = models.CharField(choices=themes_main, default=themes_main[0], verbose_name="首頁模版", max_length=100)
    android_version = models.CharField(max_length=100)
    ios_version = models.CharField(max_length=100)
    android_app_id = models.TextField()
    ios_app_id = models.TextField()
    ios_master_secret = models.TextField()
    ios_app_key = models.TextField()
    android_master_secret = models.TextField()
    android_app_key = models.TextField()
    open_install_key = models.TextField(null=True, blank=True)
    android_mini_version = models.TextField(null=True, blank=True)
    ios_mini_version = models.TextField(null=True, blank=True)
    android_short_version = models.TextField(null=True, blank=True)
    ios_short_version = models.TextField(null=True, blank=True)
    ios_xcode_target = models.TextField(choices=xcode_target, default=xcode_target[0], verbose_name="XCode Target", max_length=100)
    android_gradle_module = models.TextField()
    android_appsflyer_dev_code = models.TextField(null=True, blank=True)    
    android_market_code = models.TextField(null=True, blank=True)   

    class Meta:
        verbose_name = '平台'
        verbose_name_plural = '菲私彩App'
        db_table = 'platforms_vn'
        managed = False

class PlatformsData_Zh(models.Model):
    code = models.CharField(max_length= 100, verbose_name="平台名稱(Code)")
    platform_code = models.CharField(max_length= 100, verbose_name="平台代碼(Platform code)")
    ios_appname = models.CharField(max_length=100)
    android_appname = models.CharField(max_length=100)
    download_url = models.TextField(verbose_name="域名")
    download_url2 = models.TextField(verbose_name="域名2", null=True, blank=True)
    remark = models.CharField(choices=signs, default=signs[0], verbose_name="簽名方式", null=True, blank=True, max_length= 100)
    remark2 = models.TextField(verbose_name="----註解----",null=True, blank=True)
    super_user_id = models.CharField(max_length= 100, null=True, blank=True)
    invite_code = models.CharField(max_length= 100, null=True, blank=True)
    regular_customer = models.BooleanField(verbose_name="老客戶") 
    is_test_domain = models.BooleanField(verbose_name="測試用平台")
    country_theme = models.CharField(max_length=100)
    plat_default_lang = models.CharField(max_length=100)
    appearance_style = models.CharField(choices=appearances, default=appearances[0], verbose_name="外觀樣式", max_length= 100)
    app_theme = models.CharField(choices=themes_main, default=themes_main[0], verbose_name="首頁模版", max_length=100)
    android_version = models.CharField(max_length=100)
    ios_version = models.CharField(max_length=100)
    android_app_id = models.TextField()
    ios_app_id = models.TextField()
    ios_master_secret = models.TextField()
    ios_app_key = models.TextField()
    android_master_secret = models.TextField()
    android_app_key = models.TextField()
    open_install_key = models.TextField(null=True, blank=True)
    android_mini_version = models.TextField(null=True, blank=True)
    ios_mini_version = models.TextField(null=True, blank=True)
    android_short_version = models.TextField(null=True, blank=True)
    ios_short_version = models.TextField(null=True, blank=True)
    ios_xcode_target = models.TextField(choices=xcode_target, default=xcode_target[0], verbose_name="XCode Target", max_length=100)
    android_gradle_module = models.TextField()
    android_appsflyer_dev_code = models.TextField(null=True, blank=True)    
    android_market_code = models.TextField(null=True, blank=True)   

    class Meta:
        verbose_name = '平台'
        verbose_name_plural = '越南版中國彩App'
        db_table = 'platforms_vn'
        managed = False



class PlatformsData_Sp(models.Model):
    code = models.CharField(max_length=100)
    ios_appname = models.CharField(max_length=100)
    android_appname = models.CharField(max_length=100)
    download_url = models.TextField(verbose_name="域名")
    download_url2 = models.TextField(verbose_name="域名2")
    remark = models.TextField(verbose_name="註解", null=True, blank=True)
    android_version = models.CharField(max_length=100)
    ios_version = models.CharField(max_length=100)
    android_app_id = models.TextField()
    ios_app_id = models.TextField()
    ios_master_secret = models.TextField()
    ios_app_key = models.TextField()
    android_master_secret = models.TextField()
    android_app_key = models.TextField()
    open_install_key = models.TextField()

    class Meta:
        verbose_name = '平台'
        verbose_name_plural = '體育版App'
        db_table = 'platforms_sp'