from django.template import Library

register = Library()

@register.filter(name='cut')
def cut(value, arg):
    print(value)
    return value.replace(arg, '123')

@register.simple_tag
def simple_print():
    print('QQ123')
    pass