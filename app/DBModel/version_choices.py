from django.db import models
import requests

class Choices():
    headers = {
        'Authorization': 'Basic Z2l0MjczMmxhYjpmejZFYW15K1g3SypBdndr',
        'PRIVATE-TOKEN': 'glpat-eueVLn4wioHr2WWb7Acm'
    }

    def get_branches(self, projectId: int):
        CHOICES = (
            ('', "版號取得失敗"),
        )
        url = f"https://gitlab.cxgi.org/api/v4/projects/{projectId}/repository/branches/?search=^release&per_page=100"
        response = requests.request("GET", url, headers=self.headers, verify=False)
        response_json = response.json()

        try:
            # all_branches = list(f['name'] for f in response_json)
            branches = list(str(f['name']).split('/')[-1] for f in response_json if 'release' in f['name'])
            min_choice = list((b, b) for b in branches)
            min_choice.reverse()
            if len(min_choice) > 0:
                CHOICES = tuple(min_choice)
        except:
            print(response_json)

        return CHOICES