from django import forms
from .models import *

class IconForm(forms.ModelForm):

    class Meta:
        model = appIcon
        fields = ('icon_img','bg_img','circle_icon')
        # fields = '__all__'