import os
import shutil
import requests
from django.contrib import messages
from django.shortcuts import render, redirect, HttpResponse
from .forms import *
from django.core.files.storage import FileSystemStorage
from PIL import Image

# Create your views here.
def show_test_index(requests):
    return render(requests, 'index.html')

def app_icon_view(request):
    if request.method == 'POST':
        form = IconForm(request.POST, request.FILES)
        if form.is_valid():
            # form.save()
            file = request.FILES['icon_img']
            fileName =  request.POST['name'] + f".{file.name.split('.')[-1] }"

            folder = f"media/images/{ request.POST['name'] }"
            fs = FileSystemStorage(location=folder)  # defaults to   MEDIA_ROOT

            img = Image.open(file)

            try:
                img.save(f"{folder}/{fileName}")
            except FileNotFoundError as err:
                os.mkdir(folder)
                img.save(f"{folder}/{fileName}")
            except:
                return HttpResponse('can not save.')

            return redirect('success')

    else:
        form = IconForm()

    return render(request, 'index.html', { 'form' : form })

def success(request):
    return HttpResponse('Success Uploaded!')

def upload(self, request):
    print(request.POST)
    code = request.POST['upload']
    iconUploaded = True
    bgUploaded = True
    circleUploaded = True
    global iconFile, circleFile, resize_icon, launchFile, resize_img, resize_circle

    #複製平台圖片
    if ',' in code:
        c = code.split(',')
        src, dst = c[1], c[0]
        copy_image(request=request, src_code=src, dst_code=dst)
        return dst
    #icon
    try:
        iconFile = Image.open(request.FILES['icon_img'])
        size = (128, 128)
        resize_icon = Image.open(request.FILES['icon_img']).resize(size)
        save_to_local(code, 'icon', iconFile, resize_icon)
    except:
        print('no icon')
        iconUploaded = False

    #launch Image
    try:
        launchFile = Image.open(request.FILES['bg_img'])
        size = (120,204)
        resize_img = Image.open(request.FILES['bg_img']).resize(size)
        save_to_local(code, 'launchscreen', launchFile, resize_img)
    except:
        print('no bg')
        bgUploaded = False

    #circle Icon
    try:
        circleFile = Image.open(request.FILES['circle_icon'])
        size = (128, 128)
        resize_circle = Image.open(request.FILES['circle_icon']).resize(size)
        save_to_local(code, 'circle_icon', circleFile, resize_circle)
    except:
        print('no circle')
        circleUploaded = False

    msg = f'{code}平台 '
    if iconUploaded:
        msg += "Icon,"
    if bgUploaded:
        msg += "背景圖,"
    if circleUploaded:
        msg += "安卓圓形Icon,"
    msg = msg[:-1]
    msg += '上傳成功.'
    print(msg)

    if iconUploaded == False and bgUploaded == False and circleUploaded == False:
        messages.error(request, f'未選擇任何圖片或上傳失敗')
        return ""
    else:
        messages.info(request, msg)

    print('do upload\n', request.POST)
    return code

#複製平台icon
def copy_image(request, src_code, dst_code):
    src_folder = f"media/images/{src_code}"
    dst_folder = f"media/images/{dst_code}"

    try:
        for f in os.listdir(src_folder):
            src_path = src_folder + f'/{f}'
            dst_path = dst_folder + f'/{f}'
            shutil.copy(src_path, dst_path)

    except FileNotFoundError:
        os.mkdir(dst_folder)
        for f in os.listdir(src_folder):
            src_path = src_folder + f'/{f}'
            dst_path = dst_folder + f'/{f}'
            shutil.copy(src_path, dst_path)

    except:
        messages.error(request, f'複製圖片失敗')
        return

    messages.info(request, f'{src_code}平台複製圖片到{dst_code}成功.')

def save_to_local(code, fileName, file, resize_file):
    folder = f"media/images/{code}"
    try:
        file.save(f"{folder}/{fileName}.png")
        resize_file.save(f"{folder}/{fileName}_resize.png")
    except FileNotFoundError:
        os.mkdir(folder)
        file.save(f"{folder}/{fileName}.png")
        resize_file.save(f"{folder}/{fileName}_resize.png")
    except:
        print(f"{fileName} can't read file.")
