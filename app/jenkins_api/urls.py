from django.urls import path

from . import views

urlpatterns = [
    path('django_packer/', views.django_packer),
    path('django_deployer/', views.django_deployer),
    path('django_syncer/', views.django_syncer),
    path('django_rollbacker/', views.django_rollbacker),
    path('job_state/', views.job_state),
]