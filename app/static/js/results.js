checkBoxes = $('.action-checkbox');
th_codes = $('.field-code');

checkBoxes.each(function (i, element) {
    if ((i % 2) === 0) {
        element.setAttribute('style', 'background-color: white;');
    }else {
        element.setAttribute('style', 'background-color: #f9f9f9;');
    }
});

th_codes.each(function (i, element) {
    if ((i % 2) === 0) {
        element.setAttribute('style', 'background-color: white;');
    }else {
        element.setAttribute('style', 'background-color: #f9f9f9;');
    }
});

//隱藏右下按鈕
paginator = $('.paginator').find('input');
paginator.each(function (i, element) {
   element.setAttribute('style', 'visibility: hidden;');
});